from gripperIO import GripperIO

robotiq = GripperIO(17)
robotiq.activate()
robotiq.go_to(25)
robotiq.get_pos()
print( robotiq.get_gripper_status() )
print( robotiq.get_fault_status() )